/**
 * Задача 3.
 *
 * Напишите функцию `createArray`, которая будет создавать массив с заданными значениями.
 * Первым параметром функция принимает значение, которым заполнять массив.
 * А вторым — количество элементов, которое должно быть в массиве.
 *
 * Генерировать ошибки, если:
 * - В качестве первого аргумента были переданы не число, не строка, не объект и не массив;
 * - В качестве второго аргумента был передан не число.
 */

// Решение

const createArray = (value, quantity) => {
    try {
        if (!Array.isArray(value)
            && typeof(value) !== 'number'
            && typeof(value) !== 'string'
            && typeof(value) !== 'object') throw new TypeError('Invalid first value');
        if (typeof(quantity) !== 'number') throw new TypeError('Second parametr must be a number');
        const array = [];
        for (let i = 0; i < quantity; i++) {
            array.push(value);
        };
        return array;
    } catch (error) {
        return error;
    }    
};

const result = createArray('x', 5);

console.log(result); // [ x, x, x, x, x ]

exports.createArray = createArray;