/**
 * Задача 5.
 *
 * Вручную создать имплементацию функции `reduce`.
 * Логика работы ручной имплементации должна быть такой-же,
 * как и у встроенного метода.
 *
 * Заметки:
 * - Встроенные методы Array.prototype.reduce и Array.prototype.reduceRight использовать запрещено;
 * - Третий аргумент функции reduce является не обязательным;
 * - Если третий аргумент передан — он станет начальным значением аккумулятора;
 * - Если третий аргумент не передан — начальным значением аккумулятора станет первый элемент обрабатываемого массива.
 *
 * Генерировать ошибки, если:
 * - В качестве первого аргумента был передан не массив;
 * - В качестве второго аргумента была передана не функция;
 */

const array = [1, 2, 3, 4, 5];
const INITIAL_ACCUMULATOR = 6;

// Решение

const required = () => {throw new Error('required parameter')};

const reduce = (array = required(), callback = required(), number = array[0]) => {
    try {
        if (!Array.isArray(array)) throw new TypeError ('first argument must be an array');
        if (typeof(callback) !== 'function') throw new TypeError ('second argument must be a function');
        if (typeof(number) !== 'number') throw new TypeError ('third argument must be a number');
        let result = number;
        for (let index = 0; index < array.length; index++) {
            if (callback(array[index], index, array)) {
                result = callback(result, array[index], index, array);
            };
        };
        return result;
    } catch (error) {
        return error;
    }
	
};

const result = reduce(
    array,
    (accumulator, element, index, arrayRef) => {
        console.log(`${index}:`, accumulator, element, arrayRef);

        return accumulator + element;
    },
    INITIAL_ACCUMULATOR,
);

console.log(result); // 21