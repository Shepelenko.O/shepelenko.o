/**
 * Задача 2.
 *
 * Вручную создать имплементацию функции `filter`.
 * Логика работы ручной имплементации должна быть такой-же,
 * как и у встроенного метода.
 *
 * Заметки:
 * - Встроенный метод Array.prototype.filter использовать запрещено.
 *
 * Генерировать ошибки, если:
 * - В качестве первого аргумента был передан не массив;
 * - В качестве второго аргумента была передана не функция.
 *
 * Заметки:
 * - Второй аргумент встроенного метода filter (thisArg) имплементировать не нужно.
 */

const array = ['Доброе утро!', 'Добрый вечер!', 3, 512, '#', 'До свидания!'];

// Решение

const filter = (array, callback) => {
    try {
        if (!Array.isArray(array)) throw new TypeError ('First argument must be an array!');
        if (typeof(callback) !== 'function') throw new TypeError ('Second argument must be a function!');
        const result = [];
        for (let index = 0; index < array.length; index++) {
            if (callback(array[index], index, array)) result.push(array[index]);
        };
        return result;
    } catch (error) {
        return error;
    }
};

const filteredArray = filter(array, (element, index, arrayRef) => {
    console.log(`${index}:`, element, arrayRef);

    return element === 'Добрый вечер!';
});

console.log(filteredArray); // ['Добрый вечер!']