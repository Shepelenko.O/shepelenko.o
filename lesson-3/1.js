/**
 * Задача 1.
 *
 * Исправить функцию upperCaseFirst(str).
 * Функция преобразовывает первый символ переданной строки в заглавный и возвращает новую строку.
 *
 * Условия:
 * - Функция принимает один параметр;
 * - Необходимо проверить что параметр str является строкой
 */

// РЕШЕНИЕ
function upperCaseFirst(str) {

    try {
        if (typeof str !== 'string') throw new TypeError ('TypeError : Parameter must be a string!');
        const result = str.slice(0,1).toUpperCase() + str.slice(1);
        return result;    
    } catch (error) {
        return error;
    };

}

console.log(upperCaseFirst('pitter')); // Pitter
console.log(upperCaseFirst('')); // ''