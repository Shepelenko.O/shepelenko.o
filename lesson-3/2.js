/**
 * Задача 2.
 *
 * Исправить функцию checkSpam(source, spam)
 * Функция возвращает true, если строка source содержит подстроку spam. Иначе — false.
 *
 * Условия:
 * - Функция принимает два параметра;
 * - Функция содержит валидацию входных параметров на тип string.
 * - Функция должна быть нечувствительна к регистру
 */

// РЕШЕНИЕ
function checkSpam(source, spam) {

    try {
        if (typeof source !== 'string') throw new TypeError('First parameter must be a string!');
        if (typeof spam !== 'string') throw new TypeError('Second parameter must be a string!');
        const result = source.trim().toLowerCase().includes(spam.trim().toLowerCase());
        return result;
    } catch (error) {
        return error;
    }
    
}

console.log(checkSpam('pitterXXX@gmail.com', 'xxx')); // true
console.log(checkSpam('pitterxxx@gmail.com', 'XXX')); // true
console.log(checkSpam('pitterxxx@gmail.com', 'sss')); // false