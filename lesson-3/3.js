/**
 * Задача 3.
 *
 * Исправить функцию truncate(string, maxLength).
 * Функция проверяет длину строки string.
 * Если она превосходит maxLength – заменяет конец string на ... таким образом, чтобы её длина стала равна maxLength.
 * Результатом функции должна быть (при необходимости) усечённая строка.
 *
 * Условия:
 * - Функция принимает два параметра;
 * - Функция содержит валидацию входных параметров;
 * - Первый параметр должен обладать типом string;
 * - Второй параметр должен обладать типом number.
 */

// РЕШЕНИЕ
function truncate(string, maxLength) {
    try {
        if (typeof string !== 'string') throw new TypeError('First parameter must be a string!');
        if (typeof maxLength !== 'number' || isNaN(maxLength)) throw new TypeError('Second parameter must be a number!');
        if (string.length > maxLength){
            let str = string.slice(0, maxLength) + '...';
            return str;
        } else if (string.length <= maxLength) return string;
    } catch (error) {
        return error;
    }
}

console.log(truncate('Вот, что мне хотелось бы сказать на эту тему:', 21)); // 'Вот, что мне хотел...'