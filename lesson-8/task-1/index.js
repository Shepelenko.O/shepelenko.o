/**
 * Создать форму динамически при помощи JavaScript.
 * 
 * В html находится пример формы которая должна быть сгенерирована.
 * 
 * Для того что бы увидеть результат откройте index.html файл в браузере.
 * 
 * Обязательно!
 * 1. Для генерации элементов обязательно использовать метод document.createElement
 * 2. Для установки атрибутов элементам обязательно необходимо использовать document.setAttribute
 * 3. Всем созданным элементам необходимо добавить классы как в разметке
 * 4. После того как динамическая разметка будет готова необходимо удалить код в HTML который находится между комментариями
*/

// РЕШЕНИЕ

const parent = document.getElementById('form');
// form-group 1
const formGroup1 = document.createElement('div');
formGroup1.setAttribute('class', 'form-group');
parent.append(formGroup1);

// label for email
const labelEmail = document.createElement('label');
labelEmail.setAttribute('for', 'email');
labelEmail.innerHTML = 'Электропочта';
formGroup1.append(labelEmail);

// email
const inputEmail = document.createElement('input');
inputEmail.setAttribute('type', 'email');
inputEmail.setAttribute('class', 'form-control');
inputEmail.setAttribute('id', 'email');
inputEmail.setAttribute('placeholder','Введите свою электропочту');
formGroup1.append(inputEmail);

// form-group 2
const formGroup2 = document.createElement('div');
formGroup2.setAttribute('class', 'form-group');
parent.append(formGroup2);

// label for password
const labelPass = document.createElement('label');
labelPass.setAttribute('for', 'password');
labelPass.innerHTML = 'Пароль';
formGroup2.append(labelPass);

// password
const inputPass = document.createElement('input');
inputPass.setAttribute('type', 'password');
inputPass.setAttribute('class', 'form-control');
inputPass.setAttribute('id', 'password');
inputPass.setAttribute('placeholder','Введите пароль');
formGroup2.append(inputPass);

// form-group 3
const formGroup3 = document.createElement('div');
formGroup3.setAttribute('class', 'form-group form-check');
parent.append(formGroup3);

// checkbox
const checkbox = document.createElement('input');
checkbox.setAttribute('type', 'checkbox');
checkbox.setAttribute('class', 'form-check-input');
checkbox.setAttribute('id', 'exampleCheck1');
formGroup3.append(checkbox);

// label for checkbox
const labelCheckbox = document.createElement('label');
labelCheckbox.setAttribute('for', 'exampleCheck1');
labelCheckbox.setAttribute('class', 'form-check-label');
labelCheckbox.innerHTML = 'Запомнить меня';
formGroup3.append(labelCheckbox);

// button
const button = document.createElement('button');
button.setAttribute('class', 'btn btn-primary');
button.setAttribute('type', 'submit');
button.innerHTML = 'Вход';
parent.append(button);