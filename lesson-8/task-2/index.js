/**
 * Доработать форму из 1-го задания.
 * 
 * Добавить обработчик сабмита формы.
 * 
 * Для того что бы увидеть результат откройте index.html файл в браузере.
 * 
 * Обязательно!
 * 1. При сабмите формы страница не должна перезагружаться
 * 2. Генерировать ошибку если пользователь пытается сабмитить форму с пустыми или содержащими только пробел(ы) полями.
 * 3. Если поля формы заполнены и пользователь нажимает кнопку Вход → вывести в консоль объект следующего вида
 * {
 *  email: 'эмейл который ввёл пользователь',
 *  password: 'пароль который ввёл пользователь',
 *  remember: 'true/false'
 * }
*/

// РЕШЕНИЕ

const parent = document.getElementById('form');
// form-group 1
const formGroup1 = document.createElement('div');
formGroup1.classList.add('form-group');
parent.append(formGroup1);

// label for email
const labelEmail = document.createElement('label');
labelEmail.setAttribute('for', 'email');
labelEmail.innerHTML = 'Электропочта';
formGroup1.append(labelEmail);

// email
const inputEmail = document.createElement('input');
inputEmail.setAttribute('type', 'email');
inputEmail.classList.add('form-control');
inputEmail.setAttribute('id', 'email');
inputEmail.setAttribute('placeholder','Введите свою электропочту');
formGroup1.append(inputEmail);

// form-group 2
const formGroup2 = document.createElement('div');
formGroup2.classList.add('form-group');
parent.append(formGroup2);

// label for password
const labelPass = document.createElement('label');
labelPass.setAttribute('for', 'password');
labelPass.innerHTML = 'Пароль';
formGroup2.append(labelPass);

// password
const inputPass = document.createElement('input');
inputPass.setAttribute('type', 'password');
inputPass.classList.add('form-control');
inputPass.setAttribute('id', 'password');
inputPass.setAttribute('placeholder','Введите пароль');
formGroup2.append(inputPass);

// form-group 3
const formGroup3 = document.createElement('div');
formGroup3.setAttribute('class','form-group form-check');
parent.append(formGroup3);

// checkbox
const checkbox = document.createElement('input');
checkbox.setAttribute('type', 'checkbox');
checkbox.classList.add('form-check-input');
checkbox.setAttribute('id', 'exampleCheck1');
formGroup3.append(checkbox);

// label for checkbox
const labelCheckbox = document.createElement('label');
labelCheckbox.setAttribute('for', 'exampleCheck1');
labelCheckbox.classList.add('form-check-label');
labelCheckbox.innerHTML = 'Запомнить меня';
formGroup3.append(labelCheckbox);

// button
const button = document.createElement('button');
button.setAttribute('class','btn btn-primary');
button.setAttribute('type', 'submit');
button.innerHTML = 'Вход';
parent.append(button);

const addErrorLabel = (message, forAttr, parrentElem) => {
    const errorMessage = document.createElement('label');
    errorMessage.setAttribute('for', forAttr);
    errorMessage.classList.add('errorMessage');
    errorMessage.innerHTML = message;
    parrentElem.append(errorMessage);
};

const removeErrorLabel = () => {
    const errorLabel = document.querySelectorAll('.errorMessage');
    if (errorLabel.forEach(e => e.classList.contains('errorMessageShow'))) errorLabel.forEach(e => e.classList.remove('errorMessageShow'));
};

const showErrorLabel = () => {
    const errorLabel = document.querySelectorAll('.errorMessage');
    errorLabel.forEach(e => e.classList.add('errorMessageShow'));
};

addErrorLabel('Некорректный email', 'email', formGroup1);
addErrorLabel('Некорректный password', 'password', formGroup2);

button.addEventListener('click', e => {
    e.preventDefault();
    const re = /[ ]/;

    if(inputEmail.value === '' || inputEmail.value === re){
        removeErrorLabel();
        showErrorLabel();
    };
    if(inputPass.value === '' || inputPass.value === re){
        removeErrorLabel();
        showErrorLabel();
    } else {
        const obj = {
            email: inputEmail.value,
            password: inputPass.value,
            remember: checkbox.checked,
        };
        return console.log(obj);
    };
});