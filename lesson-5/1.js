/**
 * Задача 1.
 *
 * Создайте функцию `f`, которая возвращает куб числа, переданного в качестве аргумента.
 *
 * Условия:
 * - Генерировать ошибку, если в качестве аргумента был передан не числовой тип.
 */

// РЕШЕНИЕ

const f2 = number => {
    try {
        if (typeof number !== 'number') throw new TypeError ('Parameter must ba a number!');
        return number ** 3;
    } catch (error) {
        return error;
    }
};

console.log(f2(2)); // 8