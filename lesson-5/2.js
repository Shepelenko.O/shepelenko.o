/**
 * Задача 2.
 *
 * Создайте функцию `f`, которая возвращает сумму всех переданных числовых аргументов.
 *
 * Условия:
 * - Функция должна принимать любое количество аргументов;
 * - Генерировать ошибку, если в качестве любого входного аргумента было предано не число.
 */

// РЕШЕНИЕ

const f = function() {
    try {
        Object.values(arguments).forEach(element => {
            if (typeof element !== 'number') throw new TypeError('Parameter must be a number');
        });
        let sum = 0;
        for (let i = 0; i < arguments.length; i++){
            sum += arguments[i];
        };
        return sum;
    } catch (error) {
        return error;
    }
}


console.log(f(1, 1, 1, 2, 1, 1, 1, 1)); // 9