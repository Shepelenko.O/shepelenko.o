/**
 * Задача 6.
 *
 * Создайте функцию `isEven`, которая принимает число качестве аргумента.
 * Функция возвращает булевое значение.
 * Если число, переданное в аргументе чётное — возвращается true.
 * Если число, переданное в аргументе нечётное — возвращается false.
 *
 * Условия:
 * - Генерировать ошибку, если в качестве входного аргумента был передан не числовой тип;
 */

// РЕШЕНИЕ

const isEven = number => {
    try {
        if (typeof number !== 'number') throw new TypeError('Parameter must be a number');
        if (number % 2 === 0) {
            return true;
        } else if (number % 2 !== 0) {
            return false;
        };
    } catch (error) {
        return error;
    }
};

isEven(3); // false
isEven(4); // true