/**
 * Задача 7.
 *
 * Создайте функцию `getDivisors`, которая принимает число в качестве аргумента.
 * Функция возвращает массив его делителей (чисел, на которое делится данное число начиная от 1 и заканчивая самим собой).
 *
 * Условия:
 * - Генерировать ошибку, если в качестве входного аргумента был передан не числовой тип;
 * - Генерировать ошибку, если в качестве входного аргумента был передано число меньшее, чем 1.
 * 
 * Заметки:
 * - В решении вам понадобится использовать цикл с перебором массива.
 */

// РЕШЕНИЕ

const getDivisors = number => {
    try {
        if (typeof number !== 'number') throw new TypeError (`Invalid parameter ${number}, must be a number type!`);
        if (number < 1) throw new TypeError (`Invalid parameter ${number} - must be greater than 0!`);
        const result = [];
        for (let index = 1; index <= number; index++) {
            if (number % index === 0) {
                result.push(index);
            }
        };
        return result;
    } catch (error) {
        return error;
    }
};
getDivisors(0);
getDivisors('fg');
getDivisors(12); // [1, 2, 3, 4, 6, 12]